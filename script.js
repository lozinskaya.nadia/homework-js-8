// Теоретический вопрос
//
// Опишите своими словами, как Вы понимаете, что такое обработчик событий.
// Блок кода, обычно это функция, определяемая пользователем, которая обрабатывает, или откликается на событие(нажатия на кнопку, изменения содержимого текстового поля и тд).
// Функции обработчиков событий должны быть зарегистрированы в веб-браузере,указав тип события и цель.
//  Тогда когда в указанном целевом объекте сработает событие указанного типа, браузер вызовет  и запустит обработчик, после чего будет создано событие.
//
//
//     Задание
// Создать поле для ввода цены с валидацией.
//
//     Технические требования:
//
//     При загрузке страницы показать пользователю поле ввода (input) с надписью Price.
//     Это поле будет служить для ввода числовых значений
// Поведение поля должно быть следующим:
//
//     При фокусе на поле ввода - у него должна появиться рамка зеленого цвета.
//     При потере фокуса она пропадает.
//     Когда убран фокус с поля - его значение считывается, над полем создается span,
//     в котором должен быть выведен текст: Текущая цена: ${значение из поля ввода}.
// Рядом с ним должна быть кнопка с крестиком (X). Значение внутри поля ввода окрашивается
// в зеленый цвет.
//     При нажатии на Х - span с текстом и кнопка X должны быть удалены.
//     Значение, введенное в поле ввода, обнуляется.
//     Если пользователь ввел число меньше 0 - при потере фокуса подсвечивать поле
// ввода красной рамкой, под полем выводить фразу - Please enter correct price.
//     span со значением при этом не создается.
//
//
//     В папке img лежат примеры реализации поля ввода и создающегося span.




let inputValue = document.getElementById('price');
let error = document.getElementById('error');
let currentPrice = document.getElementById('currentPrice');
let buttonClose = document.getElementById('buttonClose');
let priceValue = document.getElementById('priceValue');


buttonClose.onclick = () => {
    currentPrice.classList.add('hide');
    error.classList.add('hide');
    inputValue.value = '';
};

inputValue.onblur = function () {
    inputValue.classList.remove('green');
    if (isNaN(inputValue.value) || inputValue.value.length === 0 || inputValue.value <=0) {
        showError();
    }else {
        priceValue.style.color ='green';
        priceValue.innerHTML = inputValue.value;
        currentPrice.classList.remove('hide');
        hideError();
    }

};


inputValue.onfocus = function () {
    inputValue.classList.add('green');
};

function showError() {
    error.classList.remove('hide');
    inputValue.classList.add('red');
    currentPrice.classList.add('hide');
    inputValue.classList.remove('green');
}

function hideError() {
    error.classList.add('hide');
    inputValue.classList.remove('red');

}
